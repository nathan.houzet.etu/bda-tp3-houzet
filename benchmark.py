import tp3
import requests
import json

index_nom = "505a7b4d10f1b247d81dca0c8e0be107af7a7aff"
index_population = "2f19b9c0b9d038927ddadac01c7f0e507b33bea9"

index_nom_pop = "4012a8a27a66af04a251ecc506ef8ddbf1049284"


def NordPopulationUnder5000_WithoutIndex():
    data = {
        "selector": {"population": {"$lt": 5000}},
        "execution_stats": True,
        "fields": ["nom", "population"],

    }
    r = requests.post(tp3.url + "nord" + "/" + "_find", data=json.dumps(data),
                      headers={"Content-Type": "application/json"}, auth=(tp3.login, tp3.password))
    print(r.url)
    return r.content


def NordPopulationUnder5000_WithIndex():
    data = {
        "selector": {"population": {"$lt": 5000}},
        "execution_stats": True,
        "fields": ["nom", "population"],
        "use_index": index_population
    }

    r = requests.post(tp3.url + "nord" + "/" + "_find", data=json.dumps(data),
                      headers={"Content-Type": "application/json"}, auth=(tp3.login, tp3.password))
    print(r.url)
    return r.content


def NordPopulationUnder5000_WithDoubleIndex():
    data = {
        "selector": {"population": {"$lt": 5000}},
        "execution_stats": True,
        "fields": ["nom", "population"],
        "use_index": index_nom_pop
    }

    r = requests.post(tp3.url + "nord" + "/" + "_find", data=json.dumps(data),
                      headers={"Content-Type": "application/json"}, auth=(tp3.login, tp3.password))
    print(r.url)
    return r.content


def FrancePopulation3000and4000_WithoutIndex():
    data = {
        "selector": {"population": {"$in"[3000,4000]}},
        "execution_stats": True,
        "fields": ["nom", "population"],

    }
    r = requests.post(tp3.url + "france" + "/" + "_find", data=json.dumps(data),
                      headers={"Content-Type": "application/json"}, auth=(tp3.login, tp3.password))
    print(r.url)
    return r.content


def FrancePopulation3000and4000_WithIndex():
    data = {
        "selector": {"population": {"$in"[3000,4000]}},
        "execution_stats": True,
        "fields": ["nom", "population"],
        "use_index": index_population
    }

    r = requests.post(tp3.url + "france" + "/" + "_find", data=json.dumps(data),
                      headers={"Content-Type": "application/json"}, auth=(tp3.login, tp3.password))
    print(r.url)
    return r.content


def FrancePopulation3000and4000_WithDoubleIndex():
    data = {
        "selector": {"population": {"$in"[3000,4000]}},
        "execution_stats": True,
        "fields": ["nom", "population"],
        "use_index": index_nom_pop
    }

    r = requests.post(tp3.url + "france" + "/" + "_find", data=json.dumps(data),
                      headers={"Content-Type": "application/json"}, auth=(tp3.login, tp3.password))
    print(r.url)
    return r.content


if __name__ == "__main__":
    print(NordPopulationUnder5000_WithoutIndex())
    print(NordPopulationUnder5000_WithIndex())
    print(NordPopulationUnder5000_WithDoubleIndex)

    print(FrancePopulation3000and4000_WithoutIndex())
    print(FrancePopulation3000and4000_WithoutIndex())
    print(FrancePopulation3000and4000_WithDoubleIndex())
