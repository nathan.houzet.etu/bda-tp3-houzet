# Une api simple géographique.

## Nathan Houzet GL G2

[Lien git](https://gitlab.univ-lille.fr/nathan.houzet.etu/bda-tp3-houzet)

### 1.  Aller sur ce site et essayer de comprendre le fonctionnement de l’API.

C'est une API permettant de récuperer les données "référentiels géographiques" nationaux.
	Cela correspond par exemple à récuperer pour une commune ou un code postale les données géographiques associées.
	Par exemple via une requete curl sur le site avec les bons paramètres, on peux récuperer les villes ayant comme code postal "59390" :
	`{"nom":"Masny","code":"59390","codeDepartement":"59","codeRegion":"32","codesPostaux":["59176"],"population":4132}`
	
Les requetes retournent un document JSON avec le ou les résultats de la requete.
	
### 2.  Trouver grâce à l’API la ville la plus peuplé de la région Auvergne-Rhones Alpes.

`curl 'https://geo.api.gouv.fr/communes?codeRegion=84&boost=population' `, cette requette nous envoie le json suivant : 
```json 
[{"nom":"L'Abergement-Clémenciat","code":"01001","codeDepartement":"01",
"codeRegion":"84","codesPostaux":["01400"],"population":767}]
```
 ### 3.  Trouver grâce à l’API, la ville la plus peuplé dont le nom débute par “Mon”
	La requete suivante curl 'https://geo.api.gouv.fr/communes?nom=%22Mon%22&boost=population&limit=1' renvoie 
```json 
[{"nom":"Montpellier","code":"34172","codeDepartement":"34",
"codeRegion":"76","codesPostaux":["34000","34080","34090","34070","34295"],"population":281613,"_score":0.11162718988272766}]
```
### 4.  À l’aide du programme jq et de l’API, sauvegarder dans un document nord.json les informations pour toutes les villes du Nord et leur code postal.

	A l'aide de la commande : curl "https://geo.api.gouv.fr/communes?codeDepartement=59" | jq > nord.json

## Installer CouchDB

vm couchdb : ssh ubuntu@172.28.101.152

admin admin

curl -s 172.28.101.152:5984 | jq

curl -u remote:remote -X PUT 172.28.101.152:5984/demo

fichier de config =  /opt/couchdb/etc/local.ini

redemarrer couchdb = sudo systemctl restart couchdb.service  

## Interaction programmatique : Utilisation de CouchDB via python 

Dans le fichier [tp3.py](tp3.py) vous trouverez les differentes fonctions permettant d'intéragir avec la VM CouchDB, ces fonctions sont   :

<ul>
<li>Créer une database </li>
<li>Supprimer une database </li>
<li>Ajouter un document </li>
<li>Retirer un document </li>
<li>Mettre à jour un document </li>
<li>Visualiser un document</li>
<li>Ajouter un index à la DB selon un champ</li>
<li>Rechercher un document (ici cherchant tout les villes du nord avec un population inférieure à 3000 hab.</li>
</ul>




## Benchmarcking : 

Le fichier [benchmark.py](benchmark.py) permet de lancer les différents benchmarks résumé dans le tableau ci-dessous.

|  | SANS INDEX | AVEC INDEX SUR POPULATION | AVEC DOUBLE INDEX (nom & pop ) | Note
| ---------- | ------------------------- | ----------------- | -----| ----|
| Villes du nord < 5000 habitants | 1,7 ms | 1,6 ms | 1,2 ms | Donnée = villes du nord
|  Villes de france entre 3000 et 4000 habitants | 4,5 ms | 4,8 | 3,8 | Sur une donnée avec toute les villes de france

Au vu de ces résultats, j'ai l'impression que les indexes simples ne sont pas très efficaces.
Je pense que pour avoir un index efficace il faut que l'index soit sur tout les champs demandés.

Sur ce benchmark le seul index efficace et le double index sur population et ville et fonctionne bien car dans les requetes, les fields selectionnées sont ces deux la.

