import json

import requests

ip = "172.28.101.152"
port = "5984"
login = "remote"
password = "remote"
url = "http://" + ip + ":" + port + "/"


def createBase( baseName):
    r = requests.put(url + baseName, auth=(login, password))
    return r.content


def deleteBase( baseName):
    r = requests.delete(url + baseName, auth=(login, password))
    return r.content


def addContent( baseName, name, content):
    r = requests.put(url +baseName+"/"+ name, data=json.dumps(content), headers={"Content-Type": "application/json"},
                     auth=(login, password))
    return r.content


def removeContent( baseName, name, rev):
    r = requests.delete(url + baseName + "/" + name + "?rev=" + rev, auth=(login, password))
    return r.content

def updateContent(baseName, name, content):
    r = requests.put(url + baseName+ name, data=json.dumps(content), headers={"Content-Type": "application/json"},
                     auth=(login, password))
    return r.content

def viewContent(baseName, name):
    r = requests.get(url + baseName+ name, auth=(login, password))
    return r.content


def loadJsonNord(basename,jsonFile):
    deleteBase(basename)
    createBase(basename)
    f=open(jsonFile)
    data= json.load(f);
    dot="."
    for city in data:
        addContent(basename, city["nom"],city)
        if len(dot)>30:
            dot="."
        else:
            dot=dot[-1]+dot+" ."
        print("adding "+dot)

    f.close()

def addindex(baseName,nameOfTheIndexField):
    index={    "index": {"fields": [nameOfTheIndexField ]}}

    r = requests.post(url + baseName +"/"+ "_index",data=json.dumps(index), headers={"Content-Type": "application/json"}, auth=(login, password))
    return r.content

def find_doc(baseName,fieldname):

    data={
        "selector":{"population":{"$lt":5000}},
        "execution_stats":True,
        "fields":["nom","population"],
        "use_index" : "2f19b9c0b9d038927ddadac01c7f0e507b33bea9"
    }

    r= requests.post(url + baseName +"/"+ "_index",data=json.dumps(data), headers={"Content-Type": "application/json"}, auth=(login, password))
    return r.content

# print(base.createBase("test2"))
# print(base.addContent("test2","chouquette",{"pain":["au", "chocolat"]}))
# print(base.removeContent("test2","chouquette","1-ae190de59cdf3ff22b50a3fdee924c24"))
#addContent("test3","chouquette",{"_id":"chouquette","pain":["au","beurre"],"_rev":"1-ae190de59cdf3ff22b50a3fdee924c24"})
# print(base.deleteBase("test2"))

# index nom : 505a7b4d10f1b247d81dca0c8e0be107af7a7aff
# index population : 2f19b9c0b9d038927ddadac01c7f0e507b33bea9
